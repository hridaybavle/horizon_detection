//////////////////////////////////////////////////////
//  DroneExamplePackage.h
//
//  Created on: Nov 04, 2016
//      Author: Hriday Bavle
//
//////////////////////////////////////////////////////


#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <stdlib.h>
#include <stdio.h>
#include <memory>
#include <fstream>
#include <iostream>

//ROS dependencies
#include "ros/ros.h"

#include "sensor_msgs/Image.h"
#include "sensor_msgs/image_encodings.h"
#include "sensor_msgs/Imu.h"
#include "geometry_msgs/Vector3Stamped.h"
#include "cv_bridge/cv_bridge.h"
#include "tf/transform_datatypes.h"

//Time Synchronizer
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

//dynamic reconfigure
#include <dynamic_reconfigure/server.h>
#include <horizon_detection/horizonConfig.h>

//Eigen
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Dense>

#define edgeThresh 1
//#define lowThreshold 20
//#define max_lowThreshold 255
#define ratio 2
#define kernel_size  3
#define sub_time_dur 0.8//0.45
#define x_offset     -6.25
#define y_offset     -7.7
#define deltaT       0.01
#define KF_state     4
#define num_measurements 2

//#define generate_csv_file
//#define   generate_images_with_imu_data

int lowThreshold;

class horizon_detection
{

public:
    horizon_detection();
    ~horizon_detection();

public:
    cv_bridge::CvImagePtr cv_thermal_image;
    cv::Mat thermal_image, thermal_image_gray;
    cv::Mat detected_edges, dst, cdst;
    std::vector<cv::Vec4i> lines;
    double roll_imu, pitch_imu, yaw_imu;
    double roll_horizon, pitch_horizon;
    double roll_horizon_filtered, pitch_horizon_filtered;
    std::ofstream roll_matlab_data, pitch_matlab_data;
    std::ofstream roll_pitch_data_with_images;

    std::ofstream new_roll_data, new_pitch_data;

    geometry_msgs::Vector3Stamped horizon_pose, horizon_pose_filtered;
    geometry_msgs::Vector3Stamped imu_pose;

    Eigen::MatrixXd x_kk, p_kk, T, F;
    Eigen::MatrixXd H, R;
    Eigen::MatrixXd  x_k1k, p_k1k;
    //Eigen::MatrixXd  z_est_k, v;
    Eigen::MatrixXd  S;
    Eigen::MatrixXd  K;
    Eigen::MatrixXd  x_k1k1, p_k1k1;
    Eigen::MatrixXd  I;
    int c_;
    char buffer_[100000];

public:
    void init();
    void open(ros::NodeHandle &n);
    void run();
    void cannyThreshold();
    void parameterCallback(horizon_detection_pkg::horizonConfig &config, uint32_t level);
    void openModel();
    void synchronizeCallback(const sensor_msgs::ImageConstPtr& image, const geometry_msgs::PoseStampedConstPtr pose);
    int findHorizonLineRow(const cv::Mat input);
    bool measurement_activation;
    std::string location;

 //Publishers and Subscribers
private:
    ros::Subscriber thermal_image_sub;
    ros::Subscriber imu_sub;
    ros::Subscriber thermal_image_sub_night_;
    void thermalImageCallback(const sensor_msgs::Image &msg);
    void imuDataCallback(const sensor_msgs::Imu &msg);
    void thermalImageCallbackNew(const sensor_msgs::Image &msg);
//    void dataSyncCallback(const geometry_msgs::Vector3StampedConstPtr &horizon_msg, const geometry_msgs::Vector3StampedConstPtr &imu_msg);
//    message_filters::Subscriber<geometry_msgs::Vector3Stamped> horizon_data_sub;
//    message_filters::Subscriber<geometry_msgs::Vector3Stamped> imu_data_sub;
//    typedef message_filters::sync_policies::ExactTime<geometry_msgs::Vector3Stamped, geometry_msgs::Vector3Stamped> MySyncPolicy;
//    std::unique_ptr<message_filters::Synchronizer<MySyncPolicy>> sync;

    ros::Publisher imu_pitch_roll_pub;
    ros::Publisher horizon_pitch_roll_pub;
    ros::Publisher horizon_pitch_roll_filter_pub;
//    ros::Publisher imu_sync_data_pub;
//    ros::Publisher horizon_sync_data_pub;
    void publishHorizonRollPitch();
    void publishHorizonRollPitchFiltered();
    void publishImuRollPitch();
    void publishNAN();
    void publishMatlabData();
    void publishImuWithImagedata();
    void publishSyncIMUdata(double roll, double pitch);
    void kalmanFilter();
};


