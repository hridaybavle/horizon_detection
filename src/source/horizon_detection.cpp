//////////////////////////////////////////////////////
//  horizon_detection.cpp
//
//  Created on: Nov 4, 2016
//      Author: Hriday Bavle
//
//////////////////////////////////////////////////////


#include "horizon_detection.h"

horizon_detection::horizon_detection()
{
    x_kk   = Eigen::MatrixXd(KF_state,1);
    x_k1k  = Eigen::MatrixXd(KF_state,1);
    x_k1k1 = Eigen::MatrixXd(KF_state,1);
    p_kk   = Eigen::MatrixXd(KF_state,KF_state);
    p_k1k  = Eigen::MatrixXd(KF_state,KF_state);
    p_k1k1 = Eigen::MatrixXd(KF_state,KF_state);
    T      = Eigen::MatrixXd(KF_state,KF_state);
    H      = Eigen::MatrixXd(KF_state,KF_state);
    F      = Eigen::MatrixXd(KF_state,KF_state);
    I      = Eigen::MatrixXd(KF_state,KF_state);
    //z_est_k, v        = Eigen::MatrixXd(2,1);
    R      = Eigen::MatrixXd(num_measurements,num_measurements);
    S      = Eigen::MatrixXd(num_measurements,num_measurements);
    K      = Eigen::MatrixXd(KF_state,num_measurements);

}

horizon_detection::~horizon_detection()
{

}

void horizon_detection::init()
{
    x_kk.setZero(), p_kk.setZero(), T.setZero(), F.setZero();
    H.setZero(), R.setZero();
    x_k1k.setZero(), p_k1k.setZero();
    //z_est_k.setZero(2,1),v.setZero(2,1);
    S.setZero(), K.setZero();
    x_k1k1.setZero(), p_k1k1.setZero();

    c_ = 0;
    location = "/home/hriday/thermal_sync_data/some_light/2017-10-03-20-17-20/roll_pitch_with_images.csv";
    measurement_activation = false;


}

void horizon_detection::open(ros::NodeHandle &n)
{
    init();

    thermal_image_sub = n.subscribe("cam0/image_raw",10, &horizon_detection::thermalImageCallback, this);
    imu_sub           = n.subscribe("mavros/imu/data",10, &horizon_detection::imuDataCallback, this);

    imu_pitch_roll_pub              = n.advertise<geometry_msgs::Vector3Stamped>("imu_roll_pitch",10, true);
    horizon_pitch_roll_pub          = n.advertise<geometry_msgs::Vector3Stamped>("horizon_roll_pitch", 10, true);
    horizon_pitch_roll_filter_pub   = n.advertise<geometry_msgs::Vector3Stamped>("horizon_roll_pitch_filtered", 10, true);

    //    imu_sync_data_pub       = n.advertise<geometry_msgs::Vector3Stamped>("imu_sync_data",1, true);
    //    horizon_sync_data_pub   = n.advertise<geometry_msgs::Vector3Stamped>("horizon_sync_data",1, true);

    //Time Synchronizer
    //    horizon_data_sub.subscribe(n, "horizon_roll_pitch",10);
    //    imu_data_sub.subscribe(n, "imu_roll_pitch",10);

    //    sync.reset(new message_filters::Synchronizer<MySyncPolicy>(MySyncPolicy(10)));
    //    sync->connectInput(horizon_data_sub, imu_data_sub);
    //    sync->registerCallback(boost::bind(&horizon_detection::dataSyncCallback, this, _1, _2));

#ifdef generate_csv_file

    roll_matlab_data.open("/home/hriday/Desktop/roll_matlab_data.csv", std::ios::out | std::ios::ate | std::ios::app);
    roll_matlab_data << "time, imuRoll, horizonRollFiltered " << std::endl;
    roll_matlab_data.close();

    pitch_matlab_data.open("/home/hriday/Desktop/pitch_matlab_data.csv", std::ios::out | std::ios::ate | std::ios::app);
    pitch_matlab_data << "time, imuPitch, horizonPitchFiltered " << std::endl;
    pitch_matlab_data.close();

#endif

#ifdef generate_images_with_imu_data
    roll_pitch_data_with_images.open(location,std::ios::out | std::ios::ate | std::ios::app);
    roll_pitch_data_with_images << "time, imuRoll, imuPitch, imageNo " << std::endl;
    roll_pitch_data_with_images.close();
#endif

    roll_pitch_data_with_images.open(location,std::ios::out | std::ios::ate | std::ios::app);
    roll_pitch_data_with_images << "time, imuRoll, imuPitch, imageNo " << std::endl;
    roll_pitch_data_with_images.close();

    openModel();

    return;

}

void horizon_detection::synchronizeCallback(const sensor_msgs::ImageConstPtr& image, const geometry_msgs::PoseStampedConstPtr pose)
{
    std::cout << "entered inside " << std::endl;
    cv_bridge::CvImagePtr new_cv_thermal_image;
    cv::Mat new_thermal_image;

    try
    {
        new_cv_thermal_image = cv_bridge::toCvCopy(image,sensor_msgs::image_encodings::MONO8);

    }
    catch (cv_bridge::Exception& e)
    {

        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }


    new_thermal_image = new_cv_thermal_image->image;

    tf::Quaternion q(pose->pose.orientation.x, pose->pose.orientation.y, pose->pose.orientation.z, pose->pose.orientation.w);
    tf::Matrix3x3 m(q);

    double new_imu_yaw, new_imu_pitch, new_imu_roll;

    m.getEulerYPR(new_imu_yaw, new_imu_pitch, new_imu_roll);

    new_imu_yaw    *=180/M_PI;
    new_imu_pitch  *=180/M_PI;
    new_imu_roll   *=180/M_PI;


    //write the synchronized image and imu data in a file
    sprintf(buffer_,"/home/hriday/thermal_sync_data/some_light/2017-10-03-20-17-20/image%u.jpg",c_);
    cv::imwrite(buffer_,new_thermal_image);
    publishSyncIMUdata(new_imu_roll,new_imu_pitch);
    c_++;


}

void horizon_detection::parameterCallback(horizon_detection_pkg::horizonConfig &config, uint32_t level)
{
    lowThreshold = config.lowThreshold;
    //std::cout << "lowThreshold" << lowThreshold << std::endl;

}


void horizon_detection::thermalImageCallback(const sensor_msgs::Image &msg)
{

    ros::Time start = ros::Time::now();

    try
    {
        cv_thermal_image = cv_bridge::toCvCopy(msg,sensor_msgs::image_encodings::MONO8);

    }
    catch (cv_bridge::Exception& e)
    {

        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }


    thermal_image_gray = cv_thermal_image->image;

    //    if(thermal_image_gray.empty())
    //        std::cout << "no image received" << std::endl;
    //    else
    {
#ifdef generate_images_with_imu_data
        sprintf(buffer_,"/home/hriday/Documents/PhD/Rosbags/rgb_thermal_inertial_rosbags_/images/2016-05-24-16-36-04_moving_scene3/image%u.jpg",c_);
        cv::imwrite(buffer_,thermal_image_gray );
        publishImuWithImagedata();
        c_++;
#endif
        //        imshow("thermal_image", thermal_image_gray);
        //        cv::waitKey(1);
    }
    ros::Time stop = ros::Time::now();
    std::cout << "processing time " <<  start - stop << std::endl;

    //cannyThreshold();

}

int horizon_detection::findHorizonLineRow(const cv::Mat input)
{
    std::vector<float> sumCol(input.rows);
    for(int i=0;i<input.rows;i++)
        sumCol[i]=cv::sum(input.row(i))[0]/input.cols;

    cv::Mat sumColMat(sumCol.size(),10,CV_32F);
    for(int i=0;i<sumCol.size()-1;i++)
        sumColMat.row(i).setTo(sumCol[i]);

    sumColMat.convertTo(sumColMat,CV_8U);

    cv::Mat derivColMat;
    cv::Sobel(sumColMat,derivColMat,CV_32F,0,1);
    convertScaleAbs( derivColMat, derivColMat );
    derivColMat.convertTo(derivColMat,CV_8U);

    cv::Mat accDerivColMat = cv::Mat::zeros(derivColMat.rows,10,CV_32S);
    for(int i=1;i<accDerivColMat.rows;i++)
        accDerivColMat.row(i).setTo(cv::sum(derivColMat.rowRange(0,i).col(0))[0]);

    for(int i=0;i<accDerivColMat.rows;i++)
        if(accDerivColMat.at<short>(i,0)>128)
            return i;

    return -1;
}

void horizon_detection::cannyThreshold()
{   
    thermal_image_gray-=30;

    cv::Mat cropped = thermal_image_gray(cv::Rect(9,1,thermal_image_gray.cols-31,thermal_image_gray.rows-2)).clone();
    cv::resize(cropped,thermal_image_gray,cv::Size(620,476),0,0,cv::INTER_CUBIC);

    cv::Mat K = (cv::Mat_<double>(3,3) << 591.049657859121680, 0, 314.872121441667900, 0, 592.289345734310700, 235.471341645862740, 0, 0, 1);
    cv::Mat D = (cv::Mat_<double>(1,5) << -0.436451365056605 , 0.223922380123782 , -0.005832153980541 , 0.002374279787539 , 0.000000000000000);

    cv::Mat undistorted;
    cv::undistort(thermal_image_gray,undistorted,K,D);
    //cv::imshow("undistorted",undistorted);

    cv::Mat filtered;
    cv::bilateralFilter(undistorted,filtered,21,21*2,21/2);
    cv::equalizeHist(filtered,undistorted);
    undistorted-=130;
    //cv::imshow("undistorted_corrected",undistorted);

    cv::Mat output = thermal_image_gray.clone();
    cv::cvtColor(output,output,CV_GRAY2BGR);

    int width = 16;
    std::vector<cv::Point> horizonPoints;
    for(int i=0;i<undistorted.cols-width;i+=width)
    {
        int currentHorizonLineRow = findHorizonLineRow(undistorted.colRange(i,i+width));
        if(currentHorizonLineRow!=-1)
        {
            horizonPoints.push_back(cv::Point(i,currentHorizonLineRow));
            //cv::circle(output,cv::Point(i,currentHorizonLineRow),5,cv::Scalar(0,255,0));
        }
    }

    cv::Vec4f horizonLine;
    if(horizonPoints.size()>2)
    {
        cv::fitLine(horizonPoints,horizonLine,CV_DIST_FAIR,0, 0.01, 0.01);
        cv::line(output, cv::Point(horizonLine[2], horizonLine[3]), cv::Point(horizonLine[2]+horizonLine[0]*100, horizonLine[3]+horizonLine[1]*100), cv::Scalar(0,0,255), 1, CV_AA);

        double m=horizonLine[1]/horizonLine[0];
        double p0x=horizonLine[2];
        double p0y=horizonLine[3];
        double b = p0y - p0x*m;

        std::vector<cv::Point2f> horizon2DPoints(3);
        horizon2DPoints[0] = cv::Point(0,b);
        horizon2DPoints[1] = cv::Point(output.cols/2.0,m*output.cols/2.0+b);
        horizon2DPoints[2] = cv::Point(output.cols,m*output.cols+b);

        double eye_altitude_m = 673;
        double horizon_altitude_m = 660;
        double horizon_distance_m = 3600;
        double FOVx=2*atan(thermal_image_gray.cols/(2*K.at<double>(0,0)));

        std::vector<cv::Point3f> horizon3DPoints(3);
        horizon3DPoints[0]= cv::Point3f(-horizon_distance_m*tan(FOVx/2.0),horizon_altitude_m,horizon_distance_m);
        horizon3DPoints[1]= cv::Point3f(0,horizon_altitude_m,horizon_distance_m);
        horizon3DPoints[2]= cv::Point3f(horizon_distance_m*tan(FOVx/2.0),horizon_altitude_m,horizon_distance_m);

        double pitch = -pitch_horizon * M_PI/180.;
        double roll = roll_horizon * M_PI/180.;
        cv::Mat R = (cv::Mat_<double>(3,3) << cos(roll), -sin(roll), 0, cos(pitch)*sin(roll), cos(pitch)*cos(roll), -sin(pitch), sin(pitch)*sin(roll), sin(pitch)*cos(roll), cos(pitch));
        cv::Mat rvec;
        cv::Rodrigues(R.inv(),rvec);
        cv::Mat tvec=(cv::Mat_<double>(3,1) << 0,-eye_altitude_m,0);
        cv::solvePnP(horizon3DPoints,horizon2DPoints,K,D,rvec,tvec,true);
        cv::Rodrigues(rvec,R);
        R=R.inv();
        cv::Mat PRY = (cv::Mat_<double>(1,3) << atan2(-R.at<double>(1,2),R.at<double>(2,2)), atan2(-R.at<double>(0,1),R.at<double>(0,0)), 0.0);
        pitch_horizon = -PRY.at<double>(0)*180./M_PI;
        roll_horizon = PRY.at<double>(1)*180./M_PI;

        //cv::circle(output,horizon2DPoints[0],5,cv::Scalar(255,127,0));
        //cv::circle(output,horizon2DPoints[1],5,cv::Scalar(255,127,0));

        std::stringstream stream1,stream2;
        stream1<<"pitch="<<pitch_horizon;
        stream2<<"roll="<<roll_horizon;
        cv::putText(output,stream1.str().c_str(),cv::Point(20,30),CV_FONT_HERSHEY_PLAIN,2.0,cv::Scalar(0,0,255));
        cv::putText(output,stream2.str().c_str(),cv::Point(20,60),CV_FONT_HERSHEY_PLAIN,2.0,cv::Scalar(0,0,255));
        publishHorizonRollPitch();
        measurement_activation = true;
    }
    else
    {
        std::cout<<"not enought points to estimate line!"<<std::endl;
        measurement_activation = false;
        pitch_horizon = 0;
        roll_horizon  = 0;
        // publishNAN();
    }

    imshow("output",output);


}

void horizon_detection::imuDataCallback(const sensor_msgs::Imu &msg)
{

    tf::Quaternion q(msg.orientation.x, msg.orientation.y, msg.orientation.z, msg.orientation.w);
    tf::Matrix3x3 m(q);

    m.getEulerYPR(yaw_imu, pitch_imu, roll_imu);

    yaw_imu    *=180/M_PI;
    pitch_imu  *=180/M_PI;
    roll_imu   *=180/M_PI;

    publishImuRollPitch();
    kalmanFilter();
    publishMatlabData();
}

void horizon_detection::publishHorizonRollPitch()
{
    ros::Time sub_time = ros::Time::now() - ros::Duration(sub_time_dur);

    horizon_pose.header.stamp = sub_time;
    horizon_pose.vector.x = 1*(roll_horizon) - 0;
    horizon_pose.vector.y = 1.0*(pitch_horizon)- 0;

    horizon_pitch_roll_pub.publish(horizon_pose);

    return;
}

void horizon_detection::publishImuRollPitch()
{

    imu_pose.header.stamp = ros::Time::now();
    imu_pose.vector.x  = roll_imu;
    imu_pose.vector.y  = pitch_imu;

    imu_pitch_roll_pub.publish(imu_pose);

    return;

}

//void horizon_detection::publishNAN()
//{
//    horizon_pose.vector.x = 0;
//    horizon_pose.vector.y = 0;
//}

void horizon_detection::publishMatlabData()
{
#ifdef generate_csv_file
    roll_matlab_data.open("/home/hriday/Desktop/roll_matlab_data.csv", std::ios::out | std::ios::ate | std::ios::app);
    roll_matlab_data << ros::Time::now() << "," << imu_pose.vector.x << "," << horizon_pose_filtered.vector.x << std::endl;
    roll_matlab_data.close();

    pitch_matlab_data.open("/home/hriday/Desktop/pitch_matlab_data.csv", std::ios::out | std::ios::ate | std::ios::app);
    pitch_matlab_data << ros::Time::now() << "," << imu_pose.vector.y << "," << horizon_pose_filtered.vector.y << std::endl;
    pitch_matlab_data.close();
#endif

}

void horizon_detection::publishImuWithImagedata()
{
#ifdef generate_images_with_imu_data
    roll_pitch_data_with_images.open(location,std::ios::out | std::ios::ate | std::ios::app);
    roll_pitch_data_with_images << ros::Time::now() << "," << imu_pose.vector.x << "," << imu_pose.vector.y << "," << "image_" << c_ << std::endl;
    roll_pitch_data_with_images.close();
#endif
}

void horizon_detection::publishSyncIMUdata(double roll, double pitch)
{
    roll_pitch_data_with_images.open(location,std::ios::out | std::ios::ate | std::ios::app);
    roll_pitch_data_with_images << ros::Time::now() << "," << roll << "," << pitch << "," << "image_" << c_ << std::endl;
    roll_pitch_data_with_images.close();
}

void horizon_detection::openModel()
{
    //Model
    //Theta(k+1)  = theta(k) + W1(k)*dt
    //W1(k+1)     = W1(k)
    //Phi(k+1)    = phi(k) + W2(k)*dt;
    //W2(k+1)     = W2(k)

    x_kk(0,0) = 0;   //pitch
    x_kk(1,0) = 0;   //angular velocity in pitch
    x_kk(2,0) = 0;   //roll
    x_kk(3,0) = 0;   //angular velocity in roll

    F(0,0)    = 1;
    F(0,1)    = 1*deltaT;
    F(1,1)    = 1;
    F(2,2)    = 1;
    F(2,3)    = 1*deltaT;
    F(3,3)    = 1;

    p_kk(0,0) = 10;
    p_kk(1,1) = 10;
    p_kk(2,2) = 10;
    p_kk(3,3) = 10;

    T(0,0)    = 0.01;
    T(1,1)    = 0.01;
    T(2,2)    = 0.01;
    T(3,3)    = 0.01;

    R(0,0)    = 0.01; //pitch
    R(1,1)    = 0.01; //roll
}

void horizon_detection::kalmanFilter()
{

    x_k1k           = F*x_kk;
    p_k1k           = F*p_kk*(F.transpose().eval())+ T;

    int num_measurement_enabled = 0;

    Eigen::VectorXd z_est_k;
    Eigen::VectorXd v;
    Eigen::MatrixXd H_enabled;
    Eigen::MatrixXd R_enabled;

    if(measurement_activation == true)
        num_measurement_enabled = 2;

    z_est_k.resize(num_measurement_enabled);
    z_est_k.setZero();
    v.resize(num_measurement_enabled);
    v.setZero();
    H_enabled.resize(num_measurement_enabled, KF_state);
    H_enabled.setZero();
    R_enabled.resize(num_measurement_enabled, num_measurement_enabled);
    R_enabled.setZero();

    if(measurement_activation == true)
    {
        z_est_k(0)    = x_kk(0,0);
        z_est_k(1)    = x_kk(2,0);

        H_enabled(0,0)  = 1;
        H_enabled(1,2)  = 1;

        z_est_k   = H_enabled*x_kk;

        v(0)            = pitch_horizon - z_est_k(0);
        v(1)            = roll_horizon  - z_est_k(1);

        R_enabled = R;
    }

    //     std::cout << "H_enabled " << H_enabled << std::endl;
    //     std::cout << "R_enabled " << R_enabled << std::endl;
    //     std::cout << "v " << v << std::endl;

    S=H_enabled*p_k1k*(H_enabled.transpose().eval()) + R_enabled;

    //std::cout << "S" << S << std::endl;

    K=p_k1k*(H_enabled.transpose().eval())*(S.inverse());

    //std::cout << "K*v" << K*v << std::cout;

    x_k1k1=x_k1k + K*v;

    //std::cout << "x_k1k1 " << x_k1k1 << std::endl;

    p_k1k1=((I.setIdentity(KF_state, KF_state))-K*H_enabled)*p_k1k;

    //Next iteration
    x_kk=x_k1k1;
    p_kk=p_k1k1;

    // std::cout << "x_kk " << x_kk << std::endl;

    pitch_horizon_filtered = x_kk(0,0);
    roll_horizon_filtered  = x_kk(2,0);
    publishHorizonRollPitchFiltered();

    measurement_activation = false;
}

void horizon_detection::publishHorizonRollPitchFiltered()
{

    ros::Time sub_time = ros::Time::now() - ros::Duration(sub_time_dur);

    horizon_pose_filtered.header.stamp = sub_time;
    horizon_pose_filtered.vector.x = (roll_horizon_filtered);
    horizon_pose_filtered.vector.y = (pitch_horizon_filtered);

    horizon_pitch_roll_filter_pub.publish(horizon_pose_filtered);

}

//void horizon_detection::dataSyncCallback(const geometry_msgs::Vector3StampedConstPtr &horizon_msg, const geometry_msgs::Vector3StampedConstPtr &imu_msg)
//{
//    std::cout << "Inside the callback" << std::endl;

//    geometry_msgs::Vector3Stamped horizon_syn_data, imu_sync_data;

//    horizon_syn_data.header.stamp = ros::Time::now();
//    horizon_syn_data.vector.x = horizon_msg->vector.x;
//    horizon_syn_data.vector.y = horizon_msg->vector.y;

//    imu_sync_data.header.stamp = ros::Time::now();
//    imu_sync_data.vector.x     = imu_msg->vector.x;
//    imu_sync_data.vector.y     = imu_msg->vector.y;

//    horizon_sync_data_pub.publish(horizon_syn_data);
//    imu_sync_data_pub.publish(imu_sync_data);

//    return;
//}
