//////////////////////////////////////////////////////
//  DroneCVIARC14ROSModule_KeypointsNode.h
//
//  Created on: 29 Feb, 2016
//      Author: Hriday
//
//
//////////////////////////////////////////////////////



//I/O Stream
//std::cout
#include <iostream>

#include <string>


// ROS
#include "ros/ros.h"



//ROSModule
#include "horizon_detection.h"

using namespace std;

int main(int argc, char **argv)
{
    //Init
    ros::init(argc, argv, "horizon_detection_node"); //Say to ROS the name of the node and the parameters
    ros::NodeHandle n; //Este nodo admite argumentos!!

    std::string node_name=ros::this_node::getName();
    cout<<"node name="<<node_name<<endl;

    //Class definition
    horizon_detection horizon_detection_object;

    //Open!
    horizon_detection_object.open(n);

    //dynamic Reconfigure
    dynamic_reconfigure::Server<horizon_detection_pkg::horizonConfig> server;
    dynamic_reconfigure::Server<horizon_detection_pkg::horizonConfig>::CallbackType f;

    f = boost::bind(&horizon_detection::parameterCallback, &horizon_detection_object,_1, _2);
    server.setCallback(f);

    message_filters::Subscriber<sensor_msgs::Image> thermal_image_sub_new(n, "/thermal_cam/thermal_image_raw", 1);
    message_filters::Subscriber<geometry_msgs::PoseStamped> imu_sub_new(n, "/sbg_ellipse/imu/pose",1);

    typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::Image, geometry_msgs::PoseStamped> mySyncPolicy;

    message_filters::Synchronizer<mySyncPolicy> sync(mySyncPolicy(1000), thermal_image_sub_new, imu_sub_new);
    sync.registerCallback(boost::bind(&horizon_detection::synchronizeCallback, &horizon_detection_object, _1, _2));

    //Loop -> Ashyncronous Module
    //while(ros::ok())
    {
        ros::spin();
    }

    return 1;
}

